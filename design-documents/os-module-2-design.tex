\lstdefinestyle{customc}{
	belowcaptionskip=1\baselineskip,
	breaklines=true,
	%	frame=L,
	xleftmargin=\parindent,
	language=C,
	showstringspaces=false,
	basicstyle=\footnotesize\ttfamily,
	keywordstyle=\bfseries\color{green!40!black},
	commentstyle=\itshape\color{purple!40!black},
	identifierstyle=\color{blue},
	stringstyle=\color{orange},
}

\lstset{escapechar=@,style=customc}

\chapter{Design of Module \textit{Userprog}}

% ================================================================================= %
\section{Assignment Requirements}


\subsection{Initial Functionality}

Currently, there is no system call handling mechanism implemented in the HAL9000 operating system. Therefore, \textit{SyscallHandle()} will not execute any of the needed functionality, only returning a status STATUS\_SUCCESS. With regard to process creation, the arguments of the program are disregarded.
	      	


\subsection{Requirements}

The major requirements of the ``Userprog'' assignment, inspired from the original Pintos documentation, are the following:
\begin{itemize}
	\item \textit{System Calls for Process Management}. You have to implement the system calls \textit{SyscallProcessExit()}, \textit{SyscallProcessCreate()}, \textit{SyscallProcessGetPid()}, \textit{SyscallProcessWaitForTermination()} and \textit{SyscallProcessCloseHandle()}.
	      	      
	\item \textit{System Calls for Thread Management}. You have to implement the system calls \textit{SyscallThreadExit()}, \textit{SyscallThreadCreate()}, \textit{SyscallThreadGetTid()}, \textit{SyscallThreadWaitForTermination()} and \textit{SyscallThreadCloseHandle()}.
	      	      
	\item \textit{Program Argument Passing}. You have to change new process creation, such that the program arguments to be passed on its user-space stack.
	      	          
	\item \textit{System Calls for File System Access}. You have to implement system calls for opening existing files or creating new files (\textit{SyscallFileCreate()}), reading data from a file \textit{SyscallFileRead()} and closing a file \textit{SyscallFileClose()}).
\end{itemize}

%Some additional (and optional) requirements of the ``Userprog'' assignment, specific to UTCN / CS OSD course could be: 
%\begin{itemize}
%    \item \textit{IPC mechanisms}. You have to add in-kernel support for IPC mechanisms (pipes, shared memory) and the corresponding system calls (also including synchronization mechanisms) for user applications.
%    
%    \item \textit{Dynamic Memory Allocation Support}. Add in kernel support for mapping new areas in the application's virtual address space and also support managing dynamically allocated memory and corresponding system calls.
%    
%    \item \textit{Code Sharing Support}. Add support for sharing common code of different processes.
%   
%%     \item  \textit{Dynamically Linking Libraries}. 
%%     \item \textit{Copy-on-Write}
%\end{itemize}
%
%
%The way to allocate requirements on member teams. 
%\begin{itemize}
%    \item 3-members teams
%        \begin{enumerate}
%            \item argument passing + validation of system call arguments (pointers)
%            
%            \item system calls for process management + file system access
%            
%            \item system calls for thread management
%            
%        \end{enumerate}
%
%    \item 4-members teams (exceptional cases)
%        \begin{enumerate}
%            \item argument passing + validation of system call arguments (pointers)
%            
%            \item system calls for process management + file system access
%            
%            \item system calls for thread management
%            
%            \item IPC mechanisms
%        \end{enumerate}
%
%     \item optional subjects (for extra points)
%        \begin{itemize}
%            \item code memory sharing support
%            \item dynamic memory allocation support
%        \end{itemize}
%
%\end{itemize}


\subsection{Basic Use Cases}

\begin{enumerate}
	\item \textit{Argument passing}: Creating processes and giving them arguments directly from the command line
	\item \textit{File system and process management syscalls}: giving the users acces to basic operations on processes and files like creating, closing etc.
	\item \textit{Syscalls for thread management}: giving users access to basic operations on threads.
\end{enumerate}


% ================================================================================= %
\section{Design Description}

\subsection{Needed Data Structures and Functions}

\begin{enumerate}
	\item \textbf{Argument Passing + Validation of System Calls Arguments}. \bigskip
	      	
	      In order to implement argument passing when creating a process we should pass those parameters on the stack of that process after the process was created, so we need to modify the function \textit{\_ThreadSetupMainThreadUserStack} in order to put the parameters on the stack of the process so they can be retrieved as argc and argv when the process runs.
	      	      	
	      \begin{lstlisting}
static
STATUS
_ThreadSetupMainThreadUserStack(
    IN      PVOID               InitialStack,
    OUT     PVOID*              ResultingStack,
    IN      PPROCESS            Process
    )
{
	//put arguments on the stack
}

	      \end{lstlisting}
	      	      
	      As it was stated in the documentation, in order to access the stack of the newly created process we need access rights to it, and we can get them by mapping the corresponding physical address into the kernel space so the OS can access it. For this we use the next 2 functions to map and unmap the memory:
	      	      
	      \begin{lstlisting}
STATUS
MmuGetSystemVirtualAddressForUserBuffer(
    IN          PVOID               UserAddress,
    IN          QWORD               Size,
    IN          PAGE_RIGHTS         PageRights,
    IN          PPROCESS            Process,
    OUT         PVOID*              KernelAddress
    );
    
void
MmuFreeSystemVirtualAddressForUserBuffer(
    IN          PVOID               KernelAddress
    );
	      \end{lstlisting}
	      	      
	      Then for parsing the command line string we need to use the strtok\_s function provided by the OS to split the full command line into individual arguments.
	      	      
	      \begin{lstlisting}
const
char*
cl_strtok_s(
    When(*context == NULL, IN_Z)
    When(*context != NULL, IN_OPT)
                                char*       strToken,
    IN_Z                        char*       delimiters,
    INOUT                       char**      context
    );
	      \end{lstlisting}
	      	      
	      Then for checking the arguments passed to the system call, we will have to modify the function: 
	      
	      \begin{lstlisting}
void
SyscallHandler(
    INOUT   COMPLETE_PROCESSOR_STATE    *CompleteProcessorState
    )
{
	...
	pSyscallParameters = (PQWORD)usermodeProcessorState->RegisterValues[RegisterRbp] + 1;
	CheckSyscallParameters( pSyscallParameters );
	...
}
	      \end{lstlisting}
	      		
	      where we should make sure that every pointer is accessible by the calling process.
	To check the validity of each pointer we will use the following kernel functions:
	\begin{lstlisting}
PTR_SUCCESS
PHYSICAL_ADDRESS
VmmGetPhysicalAddressEx(
    IN      PML4                    Cr3,
    IN      PVOID                   VirtualAddress,
    OUT_OPT BOOLEAN*                Accessed,
    OUT_OPT BOOLEAN*                Dirty
    );
	\end{lstlisting}  
	which will tell us if the virtual address is actually mapped on the physical memory and the function:

	\pagebreak	
	
	\begin{lstlisting}
STATUS
VmmIsBufferValid(
    IN          PVOID                    Buffer,
    IN          QWORD                    BufferSize,
    IN          PAGE_RIGHTS              RightsRequested,
    IN          PVMM_RESERVATION_SPACE   ReservationSpace,
    IN          BOOLEAN                  KernelAccess
    );
	\end{lstlisting} 

	which checks to see if the buffer is not in the kernel space and it is mapped.

	\item \textbf{System Calls} \bigskip
	      	      	
	      We will create a new data structure for the handles containing the following fields:
	      \begin{itemize}
	      	\item Id - this field will hold an unique Id for the handle
	      	\item Type - this field will hold the type of the handle: process handle, thread handle or file handle
	      	\item Address - this field will hold the address of the child process, thread or file
	      \end{itemize}
	      
%	      \pagebreak
	      	      	
	      \begin{lstlisting}
typedef enum _HANDLE_TYPE
{
	ProcessHandle,
	ThreadHandle,
	FileHandle
} HANDLE_TYPE;		
	      \end{lstlisting}
	      	      		
	      \begin{lstlisting}	
typedef struct _HANDLE
{
	QWORD			Id;
	HANDLE_TYPE		HandleType;
	PVOID			Address;
	LIST_ENTRY		HandleList;
	
} HANDLE, *PHANDLE;
	      \end{lstlisting}		
	      	      		
	      Then we will include a list of HANDLE objects in the PROCESS structure:
	      	      	
	      \begin{lstlisting}
typedef struct _PROCESS
{
   ...
   LIST_ENTRY		AllHandlesList;
   ...
} PROCESS, *PPROCESS;
	      \end{lstlisting}
	      	      	
	      This list will hold all the handles associated with a given process. It will eventually consist of all the child processes created by this process, 
	      all the threads used by this process and all the files with which this process works.
	      	      
\end{enumerate}


\subsection{Detailed Functionality}


%Some questions you have to answer (inspired from the original Pintos design templates):
\begin{enumerate}
	\item argument passing
	      \begin{itemize}
	      	\item Briefly describe how you implemented argument parsing.  How do you arrange for the elements of argv[] to be in the right order? How do you avoid overflowing the stack page?
	      	      	      	
	      	      \begin{lstlisting}
MapUserBufferToKernelAddress(userStack);
str = fullCommandLine;
char *nextToken;
token = strtok_s(str, NumberOdArguments, " ", &nextToken);
while found(token):
	push(token,userStack);
	token = strtok_s(NULL, NumberOdArguments, " ", &nextToken);
	
AllignStackTo16Bytes();
for i in numberOfArguments:
	push address of ith token
	
push shadow space
push address of argv
push argc
push random address

UnmapUserBuffer(userStack);
	      	      \end{lstlisting}
	      	      	      	                  
	      	\item Why does \OSName{} implement \textit{strtok\_s()} but not \textit{strtok()}? 
	      	\bigskip
	      	
	      	\textit{One of the reasons why \texttt{strtok\_s()} is used instead of \texttt{strtok()} is that \texttt{strtok\_s()} is the thread-safe version of 
	      	\texttt{strtok()} and the HAL9000 operating system is a multi-threaded environment.}
	      	\bigskip
	      	      	      	                  
	      \end{itemize}
	      	      
	\item system calls
	      \begin{itemize}
	      	\item Describe how handles are associated with files, processes or threads. Are handles unique within the entire OS or just within a single process?
	      	      \bigskip
	      	      	      	                      
	      	      \textit{Handles are associated with files, processes and threads based on an unique handle ID. The handle ID is unique within the process.
	      	      	Each handle is described by the unique ID, a type which denotes that a particular handle is associated to a file, to a process, or to a
	      	      thread and by an address which holds the actual address in the memory of that file/process/thread.}
	      	      \bigskip
	      	      	      	                      
	      	\item Describe your code for reading and writing user data from the kernel.
	      	      \bigskip
	      	      	      		
	      	      \textit{When reading and writing user data from/to the kernel we always check if the pointers received as parameters in system calls are not
	      	      null, their virtual address is mapped to a physical address, they have enough rights and that they are not pointing to a kernel-space
	      	      address.}
	      	      \bigskip
	      	      	      	                      
	      	\item Suppose a system call causes a full page (4,096 bytes) of data to be copied from user space into the kernel. What is the least and the greatest possible number of inspections of the page table (e.g. calls to \textit{\_VmIsKernelAddress()}) that might result? What about for a system call that only copies 2 bytes of data? Is there room for improvement in these numbers, and how much?
	      	\bigskip
	      	      	      	
	      	\textit{Since we must validate each pointer received as a parameter in a system call in order to make sure that it is not null and that it doesn't
	      	hold an unmapped address, the number of inspections of the page table will be the number of bytes holding the data.}	      	
	      	\bigskip
	      	      	      	                      
	      	\item Briefly describe your implementation of the ``SyscallProcessWaitForTermination'' system call and how it interacts with process termination.
	      	\bigskip
	      	
	      	\textit{When a usermode application calls ``SyscallProcessWaitForTermination'', it will send the handle ID of the process it wishes to wait for to 
	      	the kernel. The ``SyscallProcessWaitForTermination'' kernel function will search in the current process' handle list for that ID and it will 
	      	retrieve the process. Then we simply just call the \texttt{ProcessWaitForTermination()} function on that process. This function will block the
	      	current process until the child process terminates. After the child process terminates, the current process will have to free all the resources 
	      	used by the child process. This is done using \texttt{ProcessCloseHandle()}.}
	      	\bigskip
	      	      	      	                      
	      	\item Any access to user program memory at a user-specified address can fail due to a bad pointer value.  Such accesses must cause the system call to fail.  System calls are fraught with such accesses, e.g. a ``SyscallFileRead'' system call requires reading the function's four arguments from the user stack then writing an arbitrary amount of user memory, and any of these can fail at any point.  This poses a design and error-handling problem: how do you best avoid obscuring the primary function of code in a morass of error-handling?  Furthermore, when an error is detected, how do you ensure that all temporarily allocated resources (locks, buffers, etc.) are freed?  In a few paragraphs, describe the strategy or strategies you adopted for managing these issues. Give an example.
	      	\bigskip
	      	
	      	\textit{The best way to avoid such cases would be to always validate the arguments and pointers of each system call before the function that
	      	executes that system call is invoked. This could be done in the \texttt{SyscallHandler()} function. We must verify that the pointers given as 
	      	arguments are not null, that they are trying to access a mapped memory location in the physical memory and that the address they are pointing 
	      	to is not a kernel address. By doing so, we can know in advance if an error might occur so that no temporary resources are allocated. For instance,
	      	if a \texttt{SyscallFileRead()} is called by the usermode and the Buffer size is 10, but the user specifies that they want to read 30 characters
	      	with the BytesToRead parameter, the \texttt{IoFileRead()} function will try to write 30 characters in the space of just 10 characters which could
	      	throw a page fault exception. If we validate all 30 addresses starting from the base address specified by the Buffer pointer we can ensure that no
	      	errors can occur when performing the file read.}
	      	\bigskip
	      	      	      	                      
	      	      	      	                      
	      	\item Consider parent process P with child process C. How do you ensure proper synchronization and avoid race conditions when P calls SyscallProcessWaitForTermination(C) before C exits?  After C exits? How do you ensure that all resources are freed in each case? How about when P terminates without waiting, before C exits? After C exits? Are there any special cases?
	      	\bigskip
	      	
	      	\textit{Proper synchronization is ensured by the implementation of \\* \texttt{ProcessWaitForTermination()} kernel function which uses events to
	      	block the parent until the child terminates, or lets the parent pass if the child has already terminated, given the fact that the event was already
	      	signaled. This way it doesn't matter if the child finished already or has yet to finish, we only care that when the parent passes the wait function,
	      	we are sure that the child has terminated, thus we can safely free the resources.}
	      	
	      	\textit{If P terminates without waiting for its children to terminate, then we should also force terminate the child processes of P. When P 
	      	terminates after C exits, we also must ensure that all the other child processes of P are terminated before actually terminating P.}
	      	\bigskip
	      	      	      	      
	      \end{itemize}
	      	      
\end{enumerate}


\subsection{Explanation of Your Design Decisions}

	We chose to add a list of handles in the process structure to keep track of all the other processes, threads and files used by a particular process. 
	By doing so, a process' handles are only visible to that process. Other processes cannot access other processes' handles, thus increasing the security
	of the operating system. We created the handle structure which always specifies the type of the handle to ensure that, for instance, a thread handle 
	cannot access a process handle. The address in the handle structure is a void pointer so that we can store different types of structures (i.e. processes, 
	threads, files) in the handle lists.
	
	We ensure that no exceptions will occur by always checking and validating the parameters received from a system call before accessing any of the kernel 
	functions. We also make sure to validate thoroughly the parameters by checking the whole range of pointers so that no address can lead to an invalid 
	memory access.


% ================================================================================= %
\section{Tests}



% ================================================================================= %
\section{Observations}

