\chapter{Design of Module \textit{Threads}}

\lstdefinestyle{customc}{
	belowcaptionskip=1\baselineskip,
	breaklines=true,
	frame=L,
	xleftmargin=\parindent,
	language=C,
	showstringspaces=false,
	basicstyle=\footnotesize\ttfamily,
	keywordstyle=\bfseries\color{green!40!black},
	commentstyle=\itshape\color{purple!40!black},
	identifierstyle=\color{blue},
	stringstyle=\color{orange},
}

\lstdefinestyle{customasm}{
	belowcaptionskip=1\baselineskip,
	frame=L,
	xleftmargin=\parindent,
	language=[x86masm]Assembler,
	basicstyle=\footnotesize\ttfamily,
	commentstyle=\itshape\color{purple!40!black},
}

\lstset{escapechar=@,style=customc}


% ================================================================================= %
\section{Assignment Requirement}


\subsection{Initial Functionality}

\begin{enumerate}
	\item \textit{Timer}.
	      When a thread wants to wait for a given specific time, the timer will execute a busy-waiting procedure that will not block it until the time has passed, so it can be put many times on the processor while waiting.
	      	      	
	\item \textit{Priority Scheduler}.
	      Currently, the HAL9000 operating system implements a basic Round Robin scheduler that doesn't take into account different threads' priorities. In other words, the CPU is given to each thread for a given amount of time, called a quantum. After the quantum is up, if the current thread did not finish its job, it is inserted back in the ready queue and the thread positioned at the head of the queue will be scheduled next. This approach can be improved if the scheduler takes into consideration threads' priorities. Threads can be inserted in the ready queue based on their priority so that higher priority threads occupy a place closer to the head of the list. In this way, higher priority threads will be scheduled first.
	      	      	
	\item \textit{Priority donation}. 
	      Since the operating system used a Round Robin scheduling algorithm the problem of priority inversion never arose, so there was no need for such a mechanism.
	      	      	
\end{enumerate}


\subsection{Requirements}

The requirements of the ``Threads'' assignment are the following:
\begin{enumerate}
	\item \textit{Timer}. Change the current implementation of the timer, named \texttt{EX\_TIMER} and located in the file ``\texttt{ex\_timer.c}'', such that to replace the busy-waiting technique with the sleep -- wakeup (block -- unblock) one. A sleeping thread is one being blocked by waiting (in a dedicated waiting queue) for some system resource to become available. 
	      %    You could use executive events (\texttt{EX\_EVENT}) to achieve this.
	      	          
	\item \textit{Priority Scheduler --- Fixed-Priority Scheduler}. Change the current Round-Robin (RR) scheduler, such that to take into account different priorities of different threads. The scheduling principle of the priority scheduler is ``any time it has to choose a thread from a thread list, the one with the highest priority must be chosen''. It must also be preemptive, which means that at any moment the processor will be allocated to the ready thread with the highest priority. The priority scheduler does not change in any way the threads' priorities (established by threads themselves) and for this reason could also be called fixed-priority-based scheduler. 
	      	      
	\item \textit{Priority Donation}. As described in the \OSName{} documentation after the fixed priority scheduler is implemented a priority inversion problem occurs. Namely, threads with medium priority may get CPU time before high priority threads which are waiting for resources owned by low priority threads. You must implement a priority donation mechanism so the low priority thread receives the priority of the highest priority thread waiting on the resources it owns. Priority nesting should also be considered, meaning that if the High priority thread is waiting for a lock owned by the medium priority thread, and the medium priority thread is waiting on a lock owned by a low priority thread, then both the low and medium priority threads should be elevated to the priority of the high priority thread.
	      	              
	      %    \item \textit{Advanced Scheduler (MLFQ) --- Dynamic-Priority Scheduler}. This is basically a priority-based scheduler, but with variable priorities, changed by the OS (scheduler) during threads execution based on their consumed processor time and waiting time. The rules to change the threads' priorities are described in the \OSName{} documentation. NOTE: This is required only for 4 people teams.
	      	      
\end{enumerate}
	

\subsection{Basic Use Cases}

\begin{enumerate}
	\item \textit{Timer}. When we want to make a thread to wait a given time period, 
	      without it blocking the current processor, letting other
	      threads to run on it
	      	      	
	\item \textit{Priority scheduling}.
	      We define three processes along with their CPU burst time and their respective priorities, as seen in table \ref{table:1}. In this example, low numbers mean high priority. The time quantum shall be 10 ms, Process P3 is executed first for 10 ms, since it has the highest priority. After the 10ms. process P1 will be executed next for another 10ms, since it has the second highest priority, and so on.
	      	      
	      \begin{table}[h!]
	      	\centering
	      	\begin{tabular}{ |c|c|c|c| } 
	      		\hline
	      		Process & Priority & Burst Time (ms) \\
	      		\hline
	      		P1      & 3        & 17              \\
	      		P2      & 1        & 23              \\
	      		P3      & 4        & 8               \\
	      			      			
	      		\hline
	      	\end{tabular}
	      	\caption{Priority Scheduler Example}
	      	\label{table:1}
	      \end{table}
	      	      
	      Starting from these premises, the sequence of thread execution will be the following:
	      	      
	      \begin{table}[h!]
	      	\centering
	      	\begin{tabular}{ |c|c|c|c|c| } 
	      		\hline
	      		No. & Process & Priority & Burst Time (ms) \\
	      		\hline
	      		1   & P3      & 4        & 8               \\
	      		2   & P1      & 3        & 10              \\
	      		3   & P2      & 1        & 10              \\
	      		4   & P1      & 3        & 7               \\
	      		5   & P2      & 1        & 10              \\
	      		6   & P2      & 1        & 3               \\
	      			      			
	      		\hline
	      	\end{tabular}
	      	\caption{Thread scheduling order} 
	      	\label{table:2}
	      \end{table}
	      	      
	      The average waiting time can be computed by adding up all waiting times for each thread and divide this value by the total number of threads:
	      \[avg = (0+8+18+28+35+45)/3 = 44.6ms\]
	      	      	
	\item \textit{Priority donation}. When the OS implements a priority based scheduling, we can use this mechanism so that the higher priority threads can get the resource they want the fastest, by donating their priority to those lower threads, so middle priority threads cannot interfere.
\end{enumerate}



% ================================================================================= %
\section{Design Description}

\subsection{Needed Data Structures and Functions}

\begin{enumerate}
		
	\item \textbf{Timer}. \bigskip
	      	        
	      In order to implement the blocking-unblocking functionality of the timer I will use the existent \texttt{EX\_EVENTS}
	      \begin{lstlisting}
typedef struct _EX_EVENT
{
	... some fields ...
} EX_EVENT, *PEX_EVENT;
	      \end{lstlisting}
	      And of course the existing timer structure on which we add
	      the previously mentioned event structure:
	      \begin{lstlisting}
typedef struct _EX_TIMER
{
	... some fields ...
			
	//This is added in order to wait and signal the 
	//passing of time
	EX_EVENT	TimedEvent;
	
	...
} EX_TIMER, *PEX_TIMER;
	      \end{lstlisting}
	      The event timer will be initialized when the timer is initialized, meaning,
	      in the ExTimerInit function, it will be used when waiting and signaling for the
	      time period.
	      This gives us the possibility to make the timer wait on an
	      event rather than busy waiting in a loop, but we also need 
	      something to trigger the event when the time has passed. For this
	      we need several more items. First we need to add a function 
	      to the timer that checks if the given time period has passed, so that
	      whoever wants to signal the event can use this function. Second, we need
	      a list and a some function to keep track of these timers and signal them when
	      needed. I think that the iomu module fits our needs, more exactly the function:
	      \begin{lstlisting}
static
BOOLEAN
(__cdecl _IomuSystemTickInterrupt)(
	IN        PDEVICE_OBJECT           Device
)
	      \end{lstlisting}
	      which will execute every time the interrupt occurs, which is perfect for us since
	      we want something to check the timers regularly. As for the list, we will add
	      a \texttt{LIST\_ENTRY} in the \texttt{iomu\_data} structure, which will keep all the timers that we need
	      and also a lock se we can synchronize the access to the list:
	      to keep track of:
	      \begin{lstlisting}
typedef struct _IOMU_DATA
{
...
LOCK		TimerListLock;
LIST_ENTRY TimerList;
...
} IOMU_DATA, *PIOMU_DATA;
	      \end{lstlisting}
	      and it will be initialized in the function IomuPreinitSystem( void ), alongside the other lists
	      from the data structure.
	      With this done, we need to do another 2 things: Populate the list of timers and update them.
	      The list will be populated by each timer that is created, meaning, when a timer is initialized, it will add
	      itself to the list of timers from \texttt{iomu\_data} structure and the timer will remove itself from the list when it 
	      is stopped or unInitialized. For this we need 2 more functions in the iomu module:
	      \begin{lstlisting}
void
IomuNewTimerCreated(
	IN EX_TIMER	Timer
)
	      \end{lstlisting}
	      and 
	      \begin{lstlisting}
STATUS
IomuRemoveTimer(
	IN EX_TIMER Timer
)
	      \end{lstlisting}
	      	      		
	      which will add and respectively remove a timer from the list.
	      	      	
	      To update the timers I mean to check if the time has passed and to signal them if so.
	      This will be done periodically in the prevoiusly mentioned ISR:
	      \begin{lstlisting}
_IomuSystemTickInterrupt( ... )
	      \end{lstlisting}
	      and for this we will need another function that scans through the timer list mentioned checking
	      if any timer has already passed and signaling it.Since it will only be used inside
	      the iomu module, we can make it static so it will be private, only for this file. The function header will look something like this:\
	      \begin{lstlisting}
static
void
_IomuCheckAllTimers(
	void
);
	      \end{lstlisting}
	      And this should be all the structures and functions that need to be modified or added for this to work.
	      In the next chapter we will see how these structures and functions come to work togheter in order to achieve our desired result.
	      	      	
	\item \textbf{Priority Scheduler.} \bigskip
	      	        
	      HAL9000's library \textbf{list.h} provides a double-linked list that can be used:
	      \begin{lstlisting}
typedef struct _LIST_ENTRY
{
	struct _LIST_ENTRY*     Flink;
	struct _LIST_ENTRY*     Blink;
} LIST_ENTRY, *PLIST_ENTRY;
	      \end{lstlisting}
	      	        
	      The \textbf{InsertOrderedList} function from \textbf{list.c} can be used to insert threads in the ready queue in the order of their priority:
	      \begin{lstlisting}
void
InsertOrderedList(
    INOUT   PLIST_ENTRY             ListHead,
    INOUT   PLIST_ENTRY             Entry,
    IN      PFUNC_CompareFunction   CompareFunction,
    IN_OPT  PVOID                   Context
    )
{
	PLIST_ENTRY pCurrentEntry;

	#ifdef DEBUG
    ASSERT(_ValidateListEntry(ListHead));
	#endif

    for (pCurrentEntry = ListHead->Flink;
         pCurrentEntry != ListHead;
         pCurrentEntry = pCurrentEntry->Flink
         )
    {
        if (CompareFunction(Entry, pCurrentEntry, Context) < 0)
        {
            // entry to insert is smaller than current entry
            break;
        }
    }

    InsertTailList(pCurrentEntry, Entry);
}
	      \end{lstlisting}
	      This function inserts an element into the list following the ordering given by the CompareFunction function. All that is left to do is to define a comparison function that can be passed as an argument to \textbf{InsertOrderedList}. The header file \textbf{list.h} already declares such a function:
	      	      
	      \begin{lstlisting}    
typedef
INT64
(__cdecl FUNC_CompareFunction) (
	IN      PLIST_ENTRY     FirstElem,
	IN      PLIST_ENTRY     SecondElem,
	IN_OPT  PVOID           Context
    );
typedef FUNC_CompareFunction*   PFUNC_CompareFunction;
	      \end{lstlisting}
	      	        
	      All that is left to do is to write the implementation of the compare function in \textbf{thread.c}:
	      	        
	      \begin{lstlisting}
PFUNC_CompareFunction
ComparePriority(
	IN      PLIST_ENTRY     FirstElem,
	IN      PLIST_ENTRY     SecondElem,
	IN_OPT  PVOID           Context
	)
{
	PTHREAD FirstThread = (PTHREAD)FirstElem;
	PTHREAD SecondThread = (PTHREAD)SecondElem;
	if (ThreadGetPriority(FirstThread) > ThreadGetPriority(FirstThread))
	{
		return 1;
	}
	else if (ThreadGetPriority(FirstThread) < ThreadGetPriority(FirstThread))
	{
		return -1;
	}
	else
	{
		return 0;
	}
}
	      \end{lstlisting}
	      	        
	\item \textbf{Priority donation}. \bigskip
	      	        
	      We will need to modify the fields of the Thread data structure in order to include a list of locks that it has acquired and a lock to assure synchronized access to it:
	      \begin{lstlisting}
	typedef struct _THREAD
{
    ... old fields ...
   //LOCK which assures synchronized access to the lockList 
   LOCK			MutexListLock;
   //A list of locks that have been acquired by this thread
   //used to compute the highest priority donated to this thread
   LIST_ENTRY	MutexList;
    ... old fields ...
} THREAD, *PTHREAD;
	      \end{lstlisting} 
	      Now that we have the Lock list we have to fill it, which will be done by modifying the function
	      \begin{lstlisting}
void
MutexAcquire(
    INOUT       PMUTEX      Mutex
    );
	      \end{lstlisting}
	      so that when a thread wants to acquire the mutex and there is no holder, besides setting itself as the holder, it will also add the lock to his \textit{LockList}. In order to save time we will use ordered insert for both the \textit{WaitingList} of the mutex and the \textit{MutexList} of the thread and for this we will need to declare and implement 2 functions for comparing mutexes and threads based on their priority
	      \begin{lstlisting}
static
INT64
_MutexCompareFunction(
IN      PLIST_ENTRY     FirstElem,
IN      PLIST_ENTRY     SecondElem,
IN_OPT  PVOID           Context
	)
{
	//return the mutex wich has in the waiting list the single thread with highest priority
}
	      \end{lstlisting}
	      for the \textit{MutexList} and
	      \begin{lstlisting}
INT64
ThreadCompareFunction(
IN      PLIST_ENTRY     FirstElem,
IN      PLIST_ENTRY     SecondElem,
IN_OPT  PVOID           Context
	)
{
	//return the thread with the higher priority
}
	      \end{lstlisting}
	      for the \textit{WaitingList} insertion.
	      	      	
	      We will also need to modify the functions for getting the priority of the thread, to take into account these changes, and releasing the mutex:
	      \begin{lstlisting}
THREAD_PRIORITY
ThreadGetPriority(
    IN_OPT  PTHREAD             Thread
    )
{
//take into account both the real priority and those of the 
//thread waiting on the locks this thread has acquired
}
	      \end{lstlisting}
	      	      
	      \begin{lstlisting}
void
MutexRelease(
    INOUT       PMUTEX      Mutex
    )
{
... old code ...
// remove this mutex from the MutexList of the thread calling it
}
	      \end{lstlisting}
	      	        
	      	        
\end{enumerate}

\newpage

\subsection{Detailed Functionality}

%Some questions you have to answer (inspired from the original Pintos design templates):
\begin{enumerate}
	\item \textbf{Timer}
	      %        \begin{itemize}
	      %            \item Briefly describe what happens in a call to \textit{ExTimerWait()}, including the effects of the timer interrupt handler.\bigskip
	      	                  
	      When calling the ExTimerWait() function, it will check to see if the timer was initialized and the time didn't already pass,
	      if so then it will call the ExEventWaitForSignal() function, having as parameter the event stored in the ExTimer data structure
	      The functionality can be seen in the next pseodocode:
	      \begin{lstlisting}
if time.initialized() and timer is not done 
	WaitForSignal
else
	return
end if
	      \end{lstlisting}
	      Like so, the whole procedure will work in the following manner:
	      \begin{enumerate}
	      		      			
	      	\item Call the ExTimerWait function;
	      	\item Return if timer uninited or time has already passed;
	      	\item Wait for the signaling of the event( our thread is switched out and put in the blocked list)
	      	\item The tick interrupt service routine occurs and checks all the timers
	      	\item Step 4 happens until our timer expires
	      	\item The Isr handler then signals our timer that it is time to wake up
	      	\item The isr removes the timer from its list
	      	\item Our thread is put in the ready list and is ready to continue execution
	      \end{enumerate}
	      	      		
	      %            \item What steps are taken to minimize the amount of time spent in the timer interrupt handler? \bigskip
	      	                  
	      Insert the timers in the list based on the time value they have to wait so that the handler can stop searching when a timer has not passed already.
	      For example:
	      \begin{enumerate}
	      	\item We have timers with the following wait values in the list: 1,2,3,4;
	      	\item The ISR is called after 2 time units have passed
	      	\item The handler checks the first timer
	      	      \begin{enumerate}
	      	      	\item it has passed
	      	      	\item signal it
	      	      	\item remove from the list
	      	      \end{enumerate}
	      	      	      	      				
	      	\item Check the second timer
	      	      \begin{enumerate}
	      	      	\item has passed
	      	      	\item signal 
	      	      	\item remove it
	      	      \end{enumerate}
	      	      	      	      				
	      	\item Check the third timer
	      	      \begin{enumerate}
	      	      	\item It has not passed
	      	      	\item Since all timers left have higher values, we can stop now
	      	      \end{enumerate}
	      	      	      	      				
	      	\item  Return from the function handling the timers, letting the handler continue its job
	      \end{enumerate}
	      %            \item How are race conditions avoided when multiple threads call \textit{ExTimerWait()} simultaneously? \bigskip
	      	                  
	      Race conditions are avoided by guarding the list with a lock, when a thread wants to add a timer to the list, then they have to acquire the lock, and wait for it if its not available.
	      	                  
	      %            \item How are race conditions avoided when a timer interrupt occurs during a call to \textit{ExTimerWait()}? 
	      	                  
	      We can prevent the interrupt to occur when we are trying to work on the timer by disabling the interrupts.
	      %        \end{itemize}
	      	          
	\item \textbf{Priority scheduler}
	      %        \begin{itemize}
	      	              
	      %            \item How do you ensure that the highest priority thread waiting for a mutex or executive event wakes up first?
	      	                  
	      We can ensure that the highest priority thread waiting for a mutex or executive event wakes up first by using the ThreadGetPriority method. We are also relying on the fact that threads are inserted in the Ready Queue in the order of their priority, so that the highest priority thread will always be at the head of the queue, ready to be scheduled first.
	      	          
	      %            \item Describe the sequence of events when a call to \textit{MutexAcquire()} causes a priority donation.  How is nested donation handled?
	      	                  
	      When a thread tries to acquire a mutex and it was already acquired by another thread, it is added to the waiting list of the mutex and this is basically donating its priority to the thread that is holding the mutex. This only becomes apparent when calling \textit{ThreadGetPriority()} at which point, the function will search through all the threads that are waiting for that specific thread to end, gets the maximum priority found and returns it as its own. This is applied recursively, also solving the nesting problem.
	      	                  
	      %            \item Describe the sequence of events when \textit{MutexRelease()} is called on a lock that a higher-priority thread is waiting for.
	      	      
	      When a thread releases a mutex, the highest priority thread is searched in the waiting list. Priorities are evaluated using \textit{ThreadGetPriority}, and once the thread with the highest priority is found, it will be unblocked.
	      	                  
	      %            \item Describe a potential race in \textit{ThreadSetPriority()} and explain how your implementation avoids it.  Can you use a lock to avoid this race?
	      Regarding any potential race conditions in \textit{ThreadSetPriority()}, since the only resource used in this function is the current thread, there is no way different threads can access that same resource and therefore no race conditions may appear.
	      	      
	      %        \end{itemize}
	      	      
	      	          
\end{enumerate}


\subsection{Explanation of Your Design Decisions}

The chosen design is very straightforward. We used already implemented data structures and functions and added a few of our own when they were needed. For the timer, we used the executive events to remove the busy waiting. For the scheduler, we used a priority-based insertion in the ready queue to make sure the highest priority thread is scheduled first, instead of searching the whole list for that thread. And finally, for the priority donation, we used the list of locks and a recursive alternative to find the highest priority from all the threads that are waiting for a mutex to be released.


% ================================================================================= %
\section{Tests}

The relevant tests for this part of the implementation are:
\begin{itemize}
	\item \texttt{test\_timer.c}
	\item \texttt{test\_priority\_sheduler.c}
	\item \texttt{test\_priority\_donation.c}
\end{itemize}

% ================================================================================= %
%\section{Observations}